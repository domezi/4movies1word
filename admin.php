<?php
include("dbcon.php");

$jwt=$_GET["jwt"];
$jwt_secret="!eK;'s;3K+8?wA!Rg'X;z_4W=2S7Nbntm/y?Ps_ak";
function jwt($user) {
    $jwt = "";
    $jwt .= base64_encode(json_encode(["alg"=>"SHA512","typ"=>"JWT"])).".";
    $jwt .= base64_encode(json_encode($user));
    return $jwt.".".hash("sha512",$jwt.$jwt_secret);
}

if(isset($_GET["jwt"])) {
    $jwt_parts = explode(".",$jwt);
    $user_encoded = json_decode(base64_decode($jwt_parts[1]));
    if($jwt == jwt($user_encoded)) {
      $_SESSION["user_id"] = $user_encoded->id;
    }
    header("location: admin.php");
    die();
}

$user=$db->query("select * from 4movies1word_users where id ='".intval($_SESSION["user_id"])."' limit 1")->fetch_object();
if($user->is_admin != 1) die("You must be logged in and have admin persmission. <a href=old.php><button>Loign</button></a>");

if($_GET["response"] != "json") {
?>
<meta charset=utf8>
<title>[Admin] Guessip!</title>
<style>body{display:none}</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
<link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>



    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">

  <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>


<nav class="navbar navbar-expand-lg navbar-light " style="background:orange"> 
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Admin Bereich</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item"> <a class="nav-link" href="?site=categories"><i class='fa fa-list'></i> Kategorien verwalten</a> </li>
        <li class="nav-item"> <a class="nav-link" href="?site=movies"><i class='fa fa-list'></i> Aufgaben verwalten</a> </li>
        <li class="nav-item"> <a class="nav-link" href="?site=users"><i class='fa fa-users'></i> User verwalten</a> </li>
      </ul>
      <div class="d-flex">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item d-none d-md-block"> <a class="nav-link" href="/"><i class='fa fa-external-link-alt'></i> Zur App</a> </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Eingeloggt als: <?php echo $user->name;?></a>
        </li>
          </ul>
      </div>
    </div>
  </div>
</nav>
<br>
<div class=container>
<?php
}
if(is_file("admin/".$_GET["site"].".php"))
include("admin/".$_GET["site"].".php");
else 
include("admin/movies.php");
?>

</div>
<style>
form{margin-bottom:0}
body {display:block}
.card {margin-bottom:1rem}
</style>
<script>
$(document).ready( function () {
    $('.table').DataTable();
} );
</script>
