<?php
include("dbcon.php");
$type=$_GET["type"];
$id=$_GET["id"];
$size=$_GET["size"];
$t=intval($_GET["t"]);
if($t<1) $t=1;

if($type=="m")
    $new='cache/'.$type.$id."-t1-".$size.".jpg";
else
    $new='cache/'.$type.$id."-".$size.".jpg";

function convert($type,$filename,$size) {

    if($filename == "") return;

    // Get new dimensions
    list($width, $height) = getimagesize($filename);
    $new_width = $size;
    $new_height = $size / $width * $height;

    // Resample
    $image_p = imagecreatetruecolor($new_width, $new_height);
    $image = imagecreatefromjpeg($filename);
    if($image == null)
        $image = imagecreatefrompng($filename);
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

    $new= 'cache/'.$type.$size.".jpg";
    //echo "written to <a target=_blank href=".$new.">".$new."</a><br>";
    imagejpeg($image_p,$new, 80);
    imagedestroy($image);
    imagedestroy($image_p);
}

if(!file_exists($new) || $_GET["force"]=="1") {
    if($type=="m") {
        $res=$db->query("select * from 4movies1word_movies where id = '".$_GET["id"]."' limit 1");
        while($row=$res->fetch_object()) {
            $db->query("update 4movies1word_movies set caches = concat(caches,',".$size."') where id = '".$_GET["id"]."'");
            convert("m".$row->id."-t1-",$row->thumbnail1,$size);
            convert("m".$row->id."-t2-",$row->thumbnail2,$size);
            convert("m".$row->id."-t3-",$row->thumbnail3,$size);
            convert("m".$row->id."-t4-",$row->thumbnail4,$size);
        }
    }
    if($type=="c") {
        $res=$db->query("select * from 4movies1word_categories where id = '".$_GET["id"]."' limit 1");
        $db->query("update 4movies1word_categories set caches = concat(caches,',".$size."') where id = '".$_GET["id"]."'");
        while($row=$res->fetch_object()) {
            convert("m".$row->id."-",$row->img_url,$size);
        }
    }
}
if($t>1)
    header("location:".str_replace("t1","t".$t,$new));
else
    header("location:".$new);