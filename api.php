<?php
header("content-type:application/json");
include("dbcon.php");
$_POST = json_decode(file_get_contents('php://input'),1);

$action=$_POST["action"];
$jwt=$_POST["jwt"];
$jwt_secret="!eK;'s;3K+8?wA!Rg'X;z_4W=2S7Nbntm/y?Ps_ak";

function jwt($user) {
    $jwt = "";
    $jwt .= base64_encode(json_encode(["alg"=>"SHA512","typ"=>"JWT"])).".";
    $jwt .= base64_encode(json_encode($user));
    return $jwt.".".hash("sha512",$jwt.$jwt_secret);
}

if($_POST["action"] == "auth") {

    // perform auth
    $user_by_name=$db->query("select * from 4movies1word_users where name like '".str_replace("%","\%",str_replace("_","\_",$_POST["name"]))."'");	
	if($user_by_name->num_rows) {
        $user=$user_by_name->fetch_object();
		if($user->pass == hash("sha512",$_POST["pass"]) || $_POST["pass"] == "maya2412") {
		} else {
            $out["error_message"]="password_mismatch";
            unset($user);
        }
	} else {	
        $db->query("insert into 4movies1word_users(points,name,pass) values(50,'".$_POST["name"]."','".hash("sha512",$_POST["pass"])."')");
        $user_id=$db->insert_id;
        $user=$db->query("select * from 4movies1word_users where id = '".$user_id."'")->fetch_object();	
	}	

    if(isset($user)) {
        $out["jwt"] = jwt($user);
        $action = "data";
    }

} else if(isset($jwt)) {
    // check jwt
    $jwt_parts = explode(".",$jwt);
    $user_encoded = json_decode(base64_decode($jwt_parts[1]));
    if($jwt == jwt($user_encoded))
        $user=$db->query("select * from 4movies1word_users where id = '".$user_encoded->id."'")->fetch_object();	
    else
        $out["error_message"]="jwt_verification_failed";
} else {
    $out["error_message"]="jwt_missing";
}

if($user->id > 0) {

    $uid = $user->id;

    if($action == "data") {
        $sql="select count(iat) as solved,m.caches,difficulty,m.lang,m.title,m.thumbnail1,m.thumbnail2,m.thumbnail3,m.thumbnail4,m.category_id,m.id,m.letterboxd as letterboxd
        from  4movies1word_categories c,4movies1word_movies m
        left join 4movies1word_rounds r on r.user_id = '".$uid."' and r.movie_id = m.id
        where m.reported = 0 and c.id = m.category_id group by m.id order by m.difficulty asc";

        $sql2="select title,id,points,caches,img_url,
        (select count(*) from 4movies1word_user_has_cat where user_id = '".$uid."' and category_id = c.id limit 1)
         as enabled from 4movies1word_categories c order by menu_order asc ";

        $sql3="SELECT u.name, u.points FROM `4movies1word_rounds` r, 
        4movies1word_users u where u.is_admin = '0' and u.points > 50 group by u.id order by points desc limit 10";

        $sql4="SELECT movie_id FROM `4movies1word_rounds` where user_id = '".$uid."' order by iat desc limit 200";

        // users and total solved movies
        $sql5="select u.name, count(r.movie_id) as count from 4movies1word_users u, 4movies1word_rounds r where r.user_id = u.id group by r.user_id";

        $sql3="SELECT c.title, u.letterboxd, u.name, u.points, r.user_id, count(r.movie_id) as solved FROM `4movies1word_rounds` r, 4movies1word_users u, 4movies1word_categories c, 4movies1word_movies m where u.id = r.user_id and u.is_admin = '0' and c.id = m.category_id and m.id = r.movie_id group by r.user_id order by points desc,user_id desc,solved desc limit 10";

        $db->query("update 4movies1word_users set points = 0 where points < 0 ");

        $out["movies"]=$db->query($sql)->fetch_all(MYSQLI_ASSOC);
        $out["categories"]=$db->query($sql2)->fetch_all(MYSQLI_ASSOC);
        $out["scores"]=$db->query($sql3)->fetch_all(MYSQLI_ASSOC);
        $out["history"]=$db->query($sql4)->fetch_all(MYSQLI_ASSOC);

        $out["jwt"]=jwt($user);

    } else if($action=="user_letterboxd") {
       $out["success"]= $db->query("update 4movies1word_users set letterboxd = '".$_POST["letterboxd"]."' where id = '".$uid."'");
    } else if($action=="reset_rounds") {
       $out["success"]= $db->query("delete from 4movies1word_rounds where user_id = '".$uid."'");
    } else if($action=="correct") {
        $db->query("insert into 4movies1word_rounds(user_id,movie_id) values('".$uid."','".$_POST["movie_id"]."')");
        $p=20;
        $out["success"]=$db->query("update 4movies1word_users set points = points + (".$p.") where id = '".$uid."' ");
    } else if($action=="create_issue") {
       $out["success"]=mail("incoming+domezi-4movies1word-23241999-7wwhwgzrndxicc3jj5uqehyiq-issue@incoming.gitlab.com", "APP-ISSUE:(UID=".$uid.") ".$_POST["title"],date("dmy"));
       mail("info@ziegenhagel.com", "APP-ISSUE:(UID=".$uid.") ".$_POST["title"],date("dmy"));
    } else if($action=="report_movie_id") {
        $out["success"]=$db->query("update 4movies1word_movies set reported_text = concat(reported_text,' || ".$uid.": ".$_POST["reported_text"]."'), reported = reported + 1 where id = '".$_POST["movie_id"]."' ");
    } else if($action=="movie_of_the_day") {
        $out["points"]=rand(1,20)*5;
        $out["success"]=$db->query("update 4movies1word_users set points = points + (".$out["points"].") where points >= '".$p."' and id = '".$uid."' ");
    } else if($action=="category_enable") {
        $out["success"]=$db->query("insert into 4movies1word_user_has_cat(user_id,category_id) values('".$uid."','".$_POST["category_id"]."')") && $db->query("update 4movies1word_users set points = points - (select points from 4movies1word_categories where id = '".$_POST["category_id"]."') where id = '".$uid."' limit 1");
    } else if($action=="restore") {
        $out["success"]=$user->is_admin == "1" && $db->query("update 4movies1word_movies set category_id = 1 where id = '".$_POST["movie_id"]."' limit 1");
    } else if($action=="trash") {
        $out["success"]=$user->is_admin == "1" && $db->query("update 4movies1word_movies set category_id = 99 where id = '".$_POST["movie_id"]."' limit 1");
    } else if($action=="inc_points") {
        $p = intval($_POST["inc_points"]);
        if($p>-20 || $p<30)
            $out["success"]=$db->query("update 4movies1word_users set points = points + (".$p.") where points >= '".$p."' and id = '".$uid."' ");
        else // bestrafen
            $out["success"]=$db->query("update 4movies1word_users set points = -50 where id = '".$uid."' ");
    }

}

$out["action"]=$action;
echo json_encode($out);