<?php
$id = intval($_GET["id"]);
header("Content-type: image/png");
header("Content-Disposition: attachment; filename=Film-AWord_".time()."_m".$id."-".$_GET["title"].".png");

$im = imagecreatetruecolor(1500,1500);

include("../dbcon.php");
$res=$db->query("select * from 4movies1word_movies where id = '".$id."' limit 1")->fetch_object();
if($res->thumbnail2=="") {
    $im_overlay     = imagecreatefromjpeg("https://filmaword.covomedia.com/cache/m".$id."-t1-600.jpg");
    imagecopyresampled($im, $im_overlay, 0, 0, 100, 0, 1500, 1500, 400, 400);
}else {
    $im_overlay     = imagecreatefromjpeg("https://filmaword.covomedia.com/cache/m".$id."-t1-600.jpg");
    imagecopyresampled($im, $im_overlay, 0, 0, 150, 0, 750, 750, 300, 300);
    $im_overlay     = imagecreatefromjpeg("https://filmaword.covomedia.com/cache/m".$id."-t2-600.jpg");
    imagecopyresampled($im, $im_overlay, 750, 0, 150, 0, 750, 750, 300, 300);
    $im_overlay     = imagecreatefromjpeg("https://filmaword.covomedia.com/cache/m".$id."-t3-600.jpg");
    imagecopyresampled($im, $im_overlay, 0, 750, 150, 0, 750, 750, 300, 300);
    $im_overlay     = imagecreatefromjpeg("https://filmaword.covomedia.com/cache/m".$id."-t4-600.jpg");
    imagecopyresampled($im, $im_overlay, 750, 750, 150, 0, 750, 750, 300, 300);
}

$im_overlay     = imagecreatefrompng("fact_post_frame.png");
imagecopyresampled($im, $im_overlay, 0, 0, 0, 0, 1500, 1500, 1500, 1500);

imagepng($im);
imagedestroy($im);
