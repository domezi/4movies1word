<?php
if($_GET["response"] == "json") {

	$out=["action"=>$_GET["action"]];

	$body = json_decode(file_get_contents('php://input'),true);
	foreach($body as $key=>$val) { $body[$key] = str_replace("'","",$val); }
	$body = json_decode(json_encode($body));

	if($_GET["action"] == "update_movie") {
		
		$movie=$body->movie;
		$out["success"]=$db->query("update 4movies1word_movies set caches = '', reported = 0, lang = '".$movie->lang."', reported_text = '".$movie->reported_text."', title = '".$movie->title."', title_de = '".$movie->title_de."', category_id = '".$movie->category_id."', difficulty = '".intval($movie->difficulty)."',  letterboxd = '".str_replace("https://letterboxd.com","",$movie->letterboxd)."', thumbnail1='".$movie->thumbnail1."', thumbnail2='".$movie->thumbnail2."', thumbnail3='".$movie->thumbnail3."', thumbnail4='".$movie->thumbnail4."' where id = '".$movie->id."' limit 1");

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "https://4movies1word.covomedia.com/img.php?type=m&force=1&size=300&id=".$movie->id); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
		curl_close($ch);  

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "https://4movies1word.covomedia.com/img.php?type=m&force=1&size=600&id=".$movie->id); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);  

	} else if($_GET["action"] == "create_movie") {
		
		$movie=$body->movie;
		$_SESSION["category_id"]=$movie->category_id;
		$out["success"]=$db->query("insert into 4movies1word_movies(lang,difficulty,letterboxd,category_id,user_id,title,title_de,thumbnail1,thumbnail2,thumbnail3,thumbnail4) values('".$movie->lang."','".(intval($movie->difficulty) <= 0 ? "5" : intval($movie->difficulty))."','".str_replace("https://letterboxd.com","",$movie->letterboxd)."','".$movie->category_id."','".$user->id."','".$movie->title."','".$movie->title_de."','".$movie->thumbnail1."','".$movie->thumbnail2."','".$movie->thumbnail3."','".$movie->thumbnail4."')");
		$out["insert_id"]=$db->insert_id;

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "https://4movies1word.covomedia.com/img.php?type=m&size=300&id=".$movie->id); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
		curl_close($ch);  

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, "https://4movies1word.covomedia.com/img.php?type=m&size=600&id=".$movie->id); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch); 



	} else if($_GET["action"] == "delete_movie") {
		
		$movie=$body->movie;
		$out["success"]=$db->query("delete from 4movies1word_movies where id = '".$movie->id."' limit 1");

	}

	die(json_encode($out));

}
?>
</div>
<div id="app">

<div id="preview_movie" @click="preview_movie=undefined" v-if="preview_movie != undefined">
	<div>
		<h2>{{preview_movie.title}}</h2>
		<div>
			<img width="48%" height="200" :src="preview_movie.thumbnail1">
			<img width="48%" height="200" :src="preview_movie.thumbnail2">
		</div><div style=margin-top:5px>
			<img width="48%" height="200" :src="preview_movie.thumbnail3">
			<img width="48%" height="200" :src="preview_movie.thumbnail4">
		</div>
	</div>	
</div>
<div id="overlay-edit" v-if="overlay.movie != undefined && overlay.mode != 'quickedit'">

	<form @submit.prevent="overlay_submit()" style="max-width:900px" method="post">
		<div style="display:flex;align-items:center;justify-content:center">
			<div style="flex:1">
				<h2 v-if="overlay.mode=='create'">Hinzufügen</h2>
				<h2 v-else>Bearbeiten: {{overlay.movie.title}}</h2>
				<div class="alert alert-info" v-if="overlay.message!=undefined">{{overlay.message}}</div>
				<select v-model="overlay.movie.category_id" required class="form-select" name="category_id">
					<option disabled value="0">Kategorie wählen</option>
					<option :value="cat.id" v-for="(cat, key) in categories" :key="key">{{cat.title}}</option>
				</select>

				<select name="lang" required v-model="overlay.movie.lang" placeholder="Sprache" class="form-control">
					<option value="" disabled>Sprache wählen</option>
					<option value="de">Deutsch</option>
					<option value="en">Englisch</option>
					<option value="mixed">Gemischte Sprachen</option>
					<option value="other">Andere Sprache </option>
				</select>

				<input name="title" autofocus required v-model="overlay.movie.title" placeholder="Gesuchtes Wort" class="form-control">
				<input name="title_de" v-model="overlay.movie.title_de" placeholder="Gesuchtes Wort (de)" class="form-control">
				<input name="thumbnail1" pattern="http(.+).jp(.*)" type="url" required v-model="overlay.movie.thumbnail1" placeholder="URL zum Bild Nr. 1 [Min. 400px, Max. 800px!! Nur JPG Datei, Kein base64]" class="form-control">
				<input name="thumbnail2" pattern="http(.+).jp(.*)" type="url" v-model="overlay.movie.thumbnail2" placeholder="URL zum Bild Nr. 2 (optional)" class="form-control">
				<input name="thumbnail3" pattern="http(.+).jp(.*)" type="url" v-model="overlay.movie.thumbnail3" placeholder="URL zum Bild Nr. 3 (optional)" class="form-control">
				<input name="thumbnail4" pattern="http(.+).jp(.*)" type="url" v-model="overlay.movie.thumbnail4" placeholder="URL zum Bild Nr. 4 (optional)" class="form-control">

				<div style="display:flex;justify-content:space-between;padding: 5px 0px" v-if="overlay.movie.thumbnail1">
					<img width="24%" v-if="overlay.movie[name].includes('http') && overlay.movie[name].includes('.jp')" height="100" style="object-fit:cover;border-radius:5px;margin:0 .5%" v-for="(name,key) in ['thumbnail1','thumbnail2','thumbnail3','thumbnail4']" :key="key" :src="overlay.movie[name]">
				</div>

				<input name="letterboxd" v-model="overlay.movie.letterboxd"  placeholder="Letterboxd Link (benötigt!)" class="form-control">
				<textarea name="difficulty" required v-model="overlay.movie.difficulty"  placeholder="Schwierigkeit (1=Bekannt, 2=Sollte man kennen, 3=Schonmal gehört,4=eher unbekannt, 5=Kenn ich nicht)" class="form-control"></textarea>
				<input v-if="overlay.mode != 'create'" name="reported_text" v-model="overlay.movie.reported_text"  placeholder="Bug Report Texte" class="form-control">
				
			</div>
			<div id="faces">
				<img :src="faces[movies.length % faces.length]" width=200 style="margin:20px">
			</div>

		</div>
		<div class="mt-3 d-flex">
			<input v-if="overlay.mode=='create'" @click="overlay.method='create_and_preview'" type="submit" value="Hinzufügen & Vorschau" class="text-white btn btn-primary">
			<input v-if="overlay.mode=='create'" @click="overlay.method='create_and_create'"  type="submit" value="Hinzufügen & Nächstes" class="text-white btn btn-primary">
			<input v-else name="update" type="submit" value="Aktualisieren" class="text-white btn btn-primary">
			<div style="margin-left:5px" class="btn btn-secondary" @click="overlay.movie=undefined">Schliessen</div>
		</div>
	</form>

</div>

<?php
//delete
if($_GET["action"]=="delete") {
	if($db->query("delete from 4movies1word_movies where id = '".$_GET["id"]."' limit 1")) {
		echo "<div class='alert alert-success'>Wurde gelöscht.</div>";
	}
}

if(isset($_POST["select_category"])) {
	$_SESSION["category_id"]=$_POST["select_category"];	
}
?>

<div class="mx-3 px-3">

		<a @click="setup_overlay_create()" class="btn btn-primary" style="float:right"><i class="fa fa-plus"></i> Hinzufügen</a>
		<h2>Aufgaben</h2>
<form method=post class="mb-3">
Aufgaben aus Kategorie: 
	<select name=select_category class="form-select d-inline-block" style="width:200px" onchange=this.form.submit()>
	<option value=0>Alle Kategorien</option>
	<?php
	$res =$db->query("select * from 4movies1word_categories");
	while($row=$res->fetch_object()) {
		echo "<option ";if($row->id==$_SESSION["category_id"])echo " selected ";echo" value='".$row->id."'>".$row->title."</option>";
	}
	?>
	</select>
</form>

<template>
  <v-data-table
    :headers="headers"
    :items="movies"
    :items-per-page="10"
	sort-by="id"
	:sort-desc="true"
	:search="search"
    class="elevation-1"
  >

  <template v-slot:top>
        <v-text-field
          v-model="search"
          label="Search"
		  item-key="id"
          class="pt-4 mx-4"
        ></v-text-field>
      </template>


  <template v-slot:item.difficulty="{ item }"><a class="quickedit" @click="quickedit(item.id,'difficulty')">{{item.difficulty}}</a></template>
  <template v-slot:item.title="{ item }"><a class="quickedit" @click="quickedit(item.id,'title')">{{item.title}}</a></template>
  <template v-slot:item.lang="{ item }"><a class="quickedit" @click="quickedit(item.id,'lang')">{{item.lang}}</a></template>
  <template v-slot:item.caches="{ item }"><a class="quickedit" @click="quickedit(item.id,'caches')">{{item.caches == "" ? "NO_CACHE":item.caches}}</a></template>
  <template v-slot:item.letterboxd="{ item }"><a target="_blank" :href="'https://letterboxd.com/'+item.letterboxd">{{item.letterboxd}}</a> <a class="quickedit" @click="quickedit(item.id,'letterboxd')">[quickedit]</a></template>
  <template v-slot:item.thumbnail1="{ item }">
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail1')" :class="{quickedit:true,error:!item.thumbnail1.match(/http(.*)\.jp(.*)/i)}" :src="item.thumbnail1" width="60" height="60"> 
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail1')" :class="{quickedit:true,error:!item.thumbnail1.match(/http(.*)\.jp(.*)/i)}" :src="'/cache/m'+item.id+'-t1-300.jpg?'+Math.random()" width="60" height="60"> 
  </template>
  <template v-slot:item.thumbnail2="{ item }">
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail2')" :class="{quickedit:true,error:!item.thumbnail2.match(/http(.*)\.jp(.*)/i)}" :src="item.thumbnail2" width="60" height="60"> 
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail2')" :class="{quickedit:true,error:!item.thumbnail2.match(/http(.*)\.jp(.*)/i)}" :src="'/cache/m'+item.id+'-t2-300.jpg?'+Math.random()" width="60" height="60"> 
  </template>
  <template v-slot:item.thumbnail3="{ item }">
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail3')" :class="{quickedit:true,error:!item.thumbnail3.match(/http(.*)\.jp(.*)/i)}" :src="item.thumbnail3" width="60" height="60"> 
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail3')" :class="{quickedit:true,error:!item.thumbnail3.match(/http(.*)\.jp(.*)/i)}" :src="'/cache/m'+item.id+'-t3-300.jpg?'+Math.random()" width="60" height="60"> 
  </template>
  <template v-slot:item.thumbnail4="{ item }">
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail4')" :class="{quickedit:true,error:!item.thumbnail4.match(/http(.*)\.jp(.*)/i)}" :src="item.thumbnail4" width="60" height="60"> 
	<img style="object-fit:cover" @click="quickedit(item.id,'thumbnail4')" :class="{quickedit:true,error:!item.thumbnail4.match(/http(.*)\.jp(.*)/i)}" :src="'/cache/m'+item.id+'-t4-300.jpg?'+Math.random()" width="60" height="60"> 
  </template>
  <template v-slot:item.category_id="{ item }"> {{categories.filter(cat=>item.category_id == cat.id)[0].title}} </template>
  <template v-slot:item.status="{ item }">
    <div class="d-flex">
        <a :href="'/admin/insta.php?id='+item.id+'&title='+item.title" class='btn btn-success'><i class='fa fa-file-download'></i></a> 
        <a @click="preview_movie=item" class='btn btn-info'><i style=color:white class='fa fa-eye'></i></a> 
        <a @click="setup_overlay_edit(item.id)" class='btn btn-primary'><i class='fa fa-edit'></i></a> 
        <a @click="delete_movie(item.id)" class='btn btn-danger'><i class='fa fa-trash'></i></a> 
    </div>
  </template>

</v-data-table>
</template>

<?php
//index
$res =$db->query("select m.*,c.title as category from 4movies1word_movies m, 4movies1word_categories c where  m.category_id = c.id and (category_id = '".$_SESSION["category_id"]."' or 0= '".intval($_SESSION["category_id"])."') order by m.id desc");
$movies=[]; while($row=$res->fetch_array()) { $movies[]=$row; }
?>

</div>
</div>
</div>

<script>
var app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
	methods: {
		quickedit: function(movie_id,field) { 
			this.overlay.message=undefined;
			this.overlay.mode="quickedit";
			this.overlay.movie=this.movie_by_id(movie_id)
			let val = prompt("Neuer Wert für Feld '"+field+"'?")
			if(val != undefined) {
				this.overlay.movie[field] = val
				this.overlay_submit()
			} else {
				alert("Ungeändert")
			}
		},
		setup_overlay_edit: function(movie_id) { this.overlay.message=undefined; this.overlay.mode="edit";this.overlay.movie=this.movie_by_id(movie_id) },
		setup_overlay_create: function() {
			this.overlay.mode="create";
			this.overlay.message=undefined;
			this.overlay.movie={
				id:"",
				title:"",
				title_de:"",
				category_id:"<?php echo intval($_SESSION["category_id"]); ?>",
				thumbnail1:"",
				thumbnail2:"",
				thumbnail3:"",
				thumbnail4:"",
				lang:"",
				letterboxd:"",
				reported_text:"",
				difficulty:"",
			}
		},
		delete_movie: function(movie_id) {
			if(confirm("Aufgabe '"+this.movie_by_id(movie_id).title+"' wirklich löschen?"))
			
			fetch("?site=movies&action=delete_movie&response=json",{
				method: "post",
				body: JSON.stringify({movie:{id:movie_id}})
			})
			.then(d=>d.json())
			.then(d=>{
				console.log("this.movies",this.movies)
				if(d.success) this.movies=this.movies.filter(m=>m.id != movie_id);
				else alert("Fehler");
			})
			.catch(e=> alert("Unbekant Fehler"))
		},
		movie_by_id: function(id) { return this.movies.filter(m=>m.id == id)[0] },
		overlay_submit: function() {
			this.overlay.message="Laden..."

			if(this.overlay.mode=="create") {

				fetch("?site=movies&action=create_movie&response=json",{
					method: "post",
					body: JSON.stringify({movie:this.overlay.movie})
				})
				.then(d=>d.json())
				.then(d=>{
					if(d.success) {

						this.overlay.movie.id=d.insert_id;
						this.movies.push(this.overlay.movie);

						if(this.overlay.method == "create_and_create")
							this.setup_overlay_create();
						else {
							this.preview_movie=this.overlay.movie
							this.overlay.movie=undefined;
						}
					} else this.overlay.message="Fehler 0x2";
				})
				.catch(e=> this.overlay.message="Unbekannter Fehler 0x1")

			} else {
				fetch("?site=movies&action=update_movie&response=json",{
					method: "post",
					body: JSON.stringify({movie:this.overlay.movie})
				})
				.then(d=>d.json())
				.then(d=>{
					if(d.success) this.overlay.movie=undefined;
					else this.overlay.message="Fehler 0x4";
				})
				.catch(e=> this.overlay.message="Unbekannter Fehler 0x3")
			}

		}
	},
	data: {
		search:"",
		categories: <?php echo json_encode($db->query("select * from 4movies1word_categories")->fetch_all(MYSQLI_ASSOC)); ?>,	
        headers: [
          { text: 'ID', value: 'id' },
          <?php if(intval($_SESSION["category_id"])==0) echo "{ text: 'Kategorie', value: 'category_id' },";?>
          { text: 'Title', value: 'title' },
          { text: 'Title_de', value: 'title_de' },
          { text: 'Lang', value: 'lang' },
          { text: 'T1', value: 'thumbnail1' },
          { text: 'T2', value: 'thumbnail2' },
          { text: 'T3', value: 'thumbnail3' },
          { text: 'T4', value: 'thumbnail4' },
          { text: 'Aktion', value: 'status' },
          { text: 'Letterboxd', value: 'letterboxd' },
          { text: 'Schwierigk.', value: 'difficulty' },
          { text: 'Flags', value: 'reported' },
          { text: 'Reports', value: 'reported_text' },
          { text: 'Caches', value: 'caches' },
        ],

		movies: <?php echo json_encode($movies);?>,
		preview_movie: undefined,
		overlay: {
			mode:"edit", // create or edit
			movie:undefined,
			method:undefined,
			message:undefined
		},
		faces: [
			"https://img.icons8.com/emoji/452/smiling-face-with-smiling-eyes.png",
			"https://cdn.pixabay.com/photo/2019/02/19/19/45/thumbs-up-4007573_960_720.png",
			"https://i.pinimg.com/originals/32/5e/ce/325eceff32c874a2cf4129c506b0e36b.png",
			"https://image.freepik.com/free-vector/happy-face-emoji-illustration_6735-1057.jpg",
			"https://img.icons8.com/emoji/452/smiling-face-with-smiling-eyes.png",
			"https://freesvg.org/img/Bug-Eyed-Tounge--Arvin61r58.png",
			"https://www.searchpng.com/wp-content/uploads/2019/02/Happy-Face-Emoji-PNG-Image-715x715.png",
			"https://www.papertraildesign.com/wp-content/uploads/2017/06/emoji-sunglasses.png",
			"https://cdn.shopify.com/s/files/1/1061/1924/products/Smiling_Face_with_Halo_grande.png?v=1571606036",
			"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUwxYANcQeRm6l3LxUng9XZy74Pw0_bJBMYQ&usqp=CAU",
			"https://co.currituck.nc.us/wp-content/uploads/exercise-happy-face.png",
			"https://wowbestpics.com/wp-content/uploads/2019/12/15875-illustration-of-an-orange-smiley-face-pv.png",
			"https://creazilla-store.fra1.digitaloceanspaces.com/cliparts/17393/happy-face-penguin-clipart-md.png",
			"https://cdn4.iconfinder.com/data/icons/beagle-1/512/beagle_sticker_smiley_emoji_emoticon_harry_potter-512.png",
			"https://www.iconarchive.com/download/i107381/google/noto-emoji-animals-nature/22278-owl.ico",
			"https://i.gifer.com/3wm5.gif",
			"https://cdn4.iconfinder.com/data/icons/teddy-bear-1/512/sticker_emoji_emoticon_teddy_smiley_harry_potter-512.png",
			"https://i.pinimg.com/originals/63/70/1e/63701eb4671cbbe4e85f082f291d574f.png",
			"https://media2.giphy.com/media/XHVerewyNrv5xswcfz/giphy.gif",
			"https://www.pngkit.com/png/full/276-2766983_choose-safer-sex-options-happy-face-emoji-icon.png",
			"https://cdn.pixabay.com/photo/2012/04/18/18/37/toy-37522_960_720.png",
			"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/03665ef2-aec1-4837-b533-7cd019e2e72a/d6o2ot5-b2bf55c4-4f12-446f-bd5f-5477b6a2e0da.png/v1/fill/w_894,h_894,strp/a_happy_smiling_face_by_vigorousjammer_d6o2ot5-pre.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD0xMjIwIiwicGF0aCI6IlwvZlwvMDM2NjVlZjItYWVjMS00ODM3LWI1MzMtN2NkMDE5ZTJlNzJhXC9kNm8yb3Q1LWIyYmY1NWM0LTRmMTItNDQ2Zi1iZDVmLTU0NzdiNmEyZTBkYS5wbmciLCJ3aWR0aCI6Ijw9MTIyMSJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.s6gDgNVf93vUZEvi6vwMzqcYjwJT9m26l8aWvG5zCKY",
			"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/SNice.svg/1200px-SNice.svg.png"
		] ,
		movies: <?php echo json_encode($movies);?>
	}
})
</script>

<style>
#overlay-edit,#preview_movie {
	position:fixed;
	top:0;right:0;	
	left:0;bottom:0;
	z-index:999;
	-webkit-backdrop-filter: blur(10px);
	background:#0009;
} #overlay-edit form,#preview_movie > div {
	max-width:700px;
	margin:auto;
	width:100%;
	margin-top:5%;
	background:#fffb;
	border-radius:10px;
	-webkit-backdrop-filter: blur(10px);
	padding:20px;
}#preview_movie > div img {
	object-fit:cover;
}
body {
	background-attachment:fixed;
	background-image:url(https://images.pexels.com/photos/189349/pexels-photo-189349.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);
	background-size:cover;
}
.d-flex .btn { display:block;width:100%}
.d-flex .btn { margin-left:5px; }
.d-flex .btn:first-of-type { margin-left:0px; }

img.error { border:5px solid cornflowerblue; }
.card {
	border:none;padding:0;
	background:none
}

td a.quickedit { min-width:80px;padding:4px}
td .quickedit { text-decoration:none;display:inline-block;text-align:center;color:#000;}
td .quickedit:hover { color:orange;cursor:pointer;animation:.2s quickedit infinite;background:#fff}
@keyframes quickedit {
	0% {transform:rotate(-1deg)}
	50% {transform:rotate(1deg)}
	100% {transform:rotate(-1deg)}
}

.form-control ,.form-select,.bg { background:#fffc;margin-bottom:3px }
.form-control:focus ,.form-select:focus { background:#fffe }
</style>


