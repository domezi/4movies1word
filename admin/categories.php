<h1>Kategorien</h1>

<div class="card card-body">
<?php
//store
if(isset($_POST["store"])) {
	if($db->query("insert into 4movies1word_categories(title,img_url) values('".$_POST["title"]."','".$_POST["img_url"]."')")) {
		echo "<div class='alert alert-success'>Wurde hinzugefügt.</div>";
	}
}
//store
if(isset($_POST["update"])) {
	if($db->query("update 4movies1word_categories set title = '".$_POST["title"]."', img_url='".$_POST["img_url"]."' where id = '".$_GET["id"]."' limit 1")) {
		echo "<div class='alert alert-success'>Wurde aktualisiert.</div>";
	}
}

//edit
if($_GET["action"] == "edit" && !isset($_POST["update"])) { 
$row=$db->query("select * from 4movies1word_categories where id = '".$_GET["id"]."' limit 1")->fetch_object();
?>
	<form method="post">
		<h2>Bearbeiten</h2>
		<input name="title" value="<?php echo $row->title;?>" placeholder="Titel der Kategorie" class="form-control">
		<input name="img_url" value="<?php echo $row->img_url;?>" placeholder="URL zum Bild" class="form-control">
		<input name="update" type="submit" value="Aktualisieren" class="btn btn-primary">
	</form>
<?php }
 //create
else { ?>
	<form method="post">
		<h2>Erstellen</h2>
		<input name="title" placeholder="Titel der Kategorie" class="form-control">
		<input name="img_url" placeholder="URL zum Bild" class="form-control">
		<input name="store" type="submit" value="Hinzufügen" class="btn btn-primary">
	</form>
<?php } ?>

</div>

<?php
//delete
if($_GET["action"]=="delete") {
	if($db->query("delete from 4movies1word_categories where id = '".$_GET["id"]."' limit 1")) {
		echo "<div class='alert alert-success'>Wurde gelöscht.</div>";
	}
}
?>

<?php
//index
$res =$db->query("select * from 4movies1word_categories");
echo "<table class='table table-hover table-striped'>";
$init = true;
while($row=$res->fetch_array()) {

	if($init) {
		echo "<thead><tr>";
		foreach($row as $key=>$val) {
			if(is_numeric($key)) continue;
			echo "<th>".$key."</th>";
		}
		echo "<th width=200>Aktionen</th>";
		echo "</tr></thead><tbody>";
		$init = false;
	}

	echo "<tr>";
	foreach($row as $key=>$val) {
		if(is_numeric($key)) continue;
		if($key == "img_url")
			echo "<td><img src='".$val."' height=100 width=100 style=object-fit:cover></td>";
		else
			echo "<td>".$val."</td>";
	}
	echo "<td>
		<a onclick='if(confirm(\"Soll diese Zeile wirklich gelöscht werden?\"))window.location.href=\"?site=".$_GET["site"]."&action=delete&id=".$row["id"]."\"' class='btn btn-danger'><i class='fa fa-trash'></i></a>
		<a href='?site=".$_GET["site"]."&action=edit&id=".$row["id"]."' class='btn btn-info'><i class='fa fa-edit'></i></a>
	</td>";
	echo "</tr>";
}
echo "</tbody></table>";
