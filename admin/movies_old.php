<?php
if($_GET["response"] == "json") {

	$out=["action"=>$_GET["action"]];

	$body = json_decode(file_get_contents('php://input'));


	if($_GET["action"] == "update_movie") {

		
		$movie=$body->movie;
		$out["success"]=$db->query("update 4movies1word_movies set  reported = 0, reported_text = '".$movie->reported_text."', title = '".$movie->title."', category_id = '".$movie->category_id."', difficulty = '".$movie->difficulty."',  letterboxd = '".$movie->letterboxd."', thumbnail1='".$movie->thumbnail1."', thumbnail2='".$movie->thumbnail2."', thumbnail3='".$movie->thumbnail3."', thumbnail4='".$movie->thumbnail4."' where id = '".$movie->id."' limit 1");

	}

	die(json_encode($out));

}
?>
</div>
<div id="app">
	<div class="m-3">
<div class="card card-body">
<?php
//store
if(isset($_POST["store"])) {
	if($db->query("insert into 4movies1word_movies(difficulty,letterboxd,category_id,user_id,title,thumbnail1,thumbnail2,thumbnail3,thumbnail4) values('".$_POST["difficulty"]."','".str_replace("https://letterboxd.com","",$_POST["letterboxd"])."','".$_POST["category_id"]."','".$user->id."','".$_POST["title"]."','".$_POST["thumbnail1"]."','".$_POST["thumbnail2"]."','".$_POST["thumbnail3"]."','".$_POST["thumbnail4"]."')")) {
		$_SESSION["category_id"]=$_POST["category_id"];
		echo "<div class='alert alert-success'>Wurde hinzugefügt.</div>";
	}
}
//store
if(isset($_POST["update"])) {
	if($db->query("update 4movies1word_movies set  reported = 0, reported_text = '".$_POST["reported_text"]."', title = '".$_POST["title"]."', category_id = '".$_POST["category_id"]."', difficulty = '".$_POST["difficulty"]."',  letterboxd = '".$_POST["letterboxd"]."', thumbnail1='".$_POST["thumbnail1"]."', thumbnail2='".$_POST["thumbnail2"]."', thumbnail3='".$_POST["thumbnail3"]."', thumbnail4='".$_POST["thumbnail4"]."' where id = '".$_GET["id"]."' limit 1")) {
		echo "<div class='alert alert-success'>Wurde aktualisiert.</div>";
	}
}

//edit
if($_GET["action"] == "edit" && !isset($_POST["update"])) { 
$row=$db->query("select * from 4movies1word_movies where id = '".$_GET["id"]."' limit 1")->fetch_object();
?>
	<form method="post">
		<h2>Bearbeiten (mit PHP und Page-reload)</h2>
		<div class="alert alert-info" v-if="overlay.message!=undefined">{{overlay.message}}</div>
		<select required class="form-select" name="category_id">
			<option selected disabled>Kategorie</option>
			<?php
			$res2 =$db->query("select * from 4movies1word_categories");
			while($row2=$res2->fetch_array()) {
				echo "<option ";if($row2["id"] == $row->category_id)echo" selected";echo" value='".$row2["id"]."'>".$row2["title"]."</option>";
			}
			?>	
		</select>
		<input name="title" value="<?php echo $row->title;?>" placeholder="Lösung der Kategorie" class="form-control">
		<input name="thumbnail1" value="<?php echo $row->thumbnail1;?>" placeholder="URL zum Bild Nr. 1" class="form-control">
		<input name="thumbnail2" value="<?php echo $row->thumbnail2;?>" placeholder="URL zum Bild Nr. 2 (optional)" class="form-control">
		<input name="thumbnail3" value="<?php echo $row->thumbnail3;?>" placeholder="URL zum Bild Nr. 3 (optional)" class="form-control">
		<input name="thumbnail4" value="<?php echo $row->thumbnail4;?>" placeholder="URL zum Bild Nr. 4 (optional)" class="form-control">
		<input name="letterboxd" value="<?php echo $row->letterboxd;?>" placeholder="Letterboxd Link (optional)" class="form-control">
		<input name="difficulty" value="<?php echo $row->difficulty;?>" placeholder="Schwierigkeit (1=Kennt jede*r, 2=Sollte man kennen, 3=schonmal gehört,4=eher unbekannt, 5=Kenn ich nicht)" class="form-control">
		<input name="reported_text" value="<?php echo $row->reported_text;?>" placeholder="Bug Report Texte" class="form-control">
		<input name="update" type="submit" value="Aktualisieren" class="btn btn-primary">
	</form>
<?php }
 //create
else { ?>
	<form class="bg p-3 " method="post">
		<h2>Aufgaben erstellen</h2>
		<div style="display:flex">
		<div style="flex:1">
		<select required class="form-select" name="category_id">
			<option selected disabled>Kategorie</option>
			<?php
			$res =$db->query("select * from 4movies1word_categories");
			while($row=$res->fetch_array()) {
				echo "<option ";if($row["id"] == $_SESSION["category_id"])echo" selected";echo" value='".$row["id"]."'>".$row["title"]."</option>";
			}
			?>	
		</select>
		<input required name="title" placeholder="Lösung der Aufgabe" autofocus class="form-control">
		<input name="thumbnail1" placeholder="URL zum Bild (max 800px!!) Nr. 1" type="url" required class="form-control">
		<input name="thumbnail2" placeholder="URL zum Bild Nr. 2 (max 800px!!) (optional)" type="url" class="form-control">
		<input name="thumbnail3" placeholder="URL zum Bild Nr. 3 (max 800px!!) (optional)" type="url" class="form-control">
		<input name="thumbnail4" placeholder="URL zum Bild Nr. 4 (max 800px!!) (optional)" type="url" class="form-control">
		<input name="letterboxd" placeholder="Letterboxd Link" required class="form-control">
		<input name="difficulty" type=number min=1 max=5 placeholder="Schwierigkeit (1=Kennt jede*r, 2=Sollte man kennen, 3=schonmal gehört,4=eher unbekannt, 5=Kenn ich nicht)" class="form-control">
		<input name="store" type="submit" value="Hinzufügen" class="btn btn-primary">
		</div>
		<?php
		if(rand(1,2)==2) {
			?>
		<div style="margin-right:5px;margin-left:20px">
			<img width="300" :src="faces[Math.floor(Math.random()*faces.length)]">
		</div>
		<?php } ?>
		</div>
	</form>
<?php } ?>

</div>
</div>

<div id="overlay-edit" v-if="overlay.movie != undefined">

	<form @submit.prevent="update()" method="post">
		<h2>Vue-Bearbeiten: {{overlay.movie.title}}</h2>
		<select required class="form-select" name="category_id">
			<option selected disabled>Kategorie</option>
			<?php
			$res2 =$db->query("select * from 4movies1word_categories");
			while($row2=$res2->fetch_array()) {
				echo "<option ";if($row2["id"] == $row->category_id)echo" selected";echo" value='".$row2["id"]."'>".$row2["title"]."</option>";
			}
			?>	
		</select>
		<input name="title" v-model="overlay.movie.title" placeholder="Lösung der Kategorie" class="form-control">
		<input name="thumbnail1" v-model="overlay.movie.thumbnail1" placeholder="URL zum Bild Nr. 1" class="form-control">
		<input name="thumbnail2" v-model="overlay.movie.thumbnail2" placeholder="URL zum Bild Nr. 2 (optional)" class="form-control">
		<input name="thumbnail3" v-model="overlay.movie.thumbnail3" placeholder="URL zum Bild Nr. 3 (optional)" class="form-control">
		<input name="thumbnail4" v-model="overlay.movie.thumbnail4" placeholder="URL zum Bild Nr. 4 (optional)" class="form-control">
		<input name="letterboxd" v-model="overlay.movie.letterboxd"  placeholder="Letterboxd Link (optional)" class="form-control">
		<input name="difficulty" v-model="overlay.movie.difficulty"  placeholder="Schwierigkeit (1=Kennt jede*r, 2=Sollte man kennen, 3=schonmal gehört,4=eher unbekannt, 5=Kenn ich nicht)" class="form-control">
		<input name="reported_text" v-model="overlay.movie.reported_text"  placeholder="Bug Report Texte" class="form-control">
		<input name="update" type="submit" value="Aktualisieren" class="btn btn-primary">
		<div class="btn btn-secondary" @click="overlay.movie=undefined">Schliessen</div>
	</form>

</div>

<?php
//delete
if($_GET["action"]=="delete") {
	if($db->query("delete from 4movies1word_movies where id = '".$_GET["id"]."' limit 1")) {
		echo "<div class='alert alert-success'>Wurde gelöscht.</div>";
	}
}

if(isset($_POST["select_category"])) {
	$_SESSION["category_id"]=$_POST["select_category"];	
}
?>

<div class="m-3 bg p-3">

		<h2>Aufgaben bearbeiten</h2>
<div class="alert alert-info mb-2"  >Die nachfolgende Tabelle aktualisiert sich nur, wenn ihr die Seite neuladet. Änderungen werden aber trotzdem übernommen, nur halt erst nach dem Seiten neuladen anzeigt. <a href="?">Komplette Seite jetzt neuladen</a> </div>
<form method=post class="mb-3">
Aufgaben aus Kategorie: 
	<select name=select_category onchange=this.form.submit()>
	<option value=0>Alle Kategorien</option>
	<?php
	$res =$db->query("select * from 4movies1word_categories");
	while($row=$res->fetch_object()) {
		echo "<option ";if($row->id==$_SESSION["category_id"])echo " selected ";echo" value='".$row->id."'>".$row->title."</option>";
	}
	?>
	</select>
</form>


<?php
//index
if($_GET["action"] == "edit" && !isset($_POST["update"])) die();  
$res =$db->query("select * from 4movies1word_movies where category_id = '".$_SESSION["category_id"]."' or 0= '".intval($_SESSION["category_id"])."' order by id desc");
echo "<table class='table table-hover table-striped'>";
$init = true;
$movies=[];
while($row=$res->fetch_array()) {

	$movies[]=$row;
	if($init) {
		echo "<thead><tr>";
		foreach($row as $key=>$val) {
			if(is_numeric($key)) continue;
			if(substr($key,0,5) == "thumb")
				echo "<th>".str_replace("humbnail","",$key)."</th>";
			else
				echo "<th>".$key."</th>";
		}
		echo "<th width=200>Aktionen</th>";
		echo "</tr></thead><tbody>";
		$init = false;
	}

	echo "<tr>";
	foreach($row as $key=>$val) {
		if(is_numeric($key)) continue;
		if(substr($key,0,5) == "thumb")
			echo "<td><img src='".$val."' height=50 width=50 style=object-fit:cover></td>";
		else if($key == "category_id")
			echo "<td>$val<br>".$db->query("select * from 4movies1word_categories where id = '".$val."' limit 1")->fetch_object()->title."</td>";
		else
			echo "<td>".$val."</td>";
	}
	echo "<td>
		<a onclick='if(confirm(\"Soll diese Zeile wirklich gelöscht werden?\"))fetch(\"?site=".$_GET["site"]."&action=delete&id=".$row["id"]."\").then(d=>alert(\"Sollte gelöscht sein\"));' class='btn btn-danger'><i class='fa fa-trash'></i></a>
		<a style=display:none href='?site=".$_GET["site"]."&action=edit&id=".$row["id"]."' class='btn btn-info'><i class='fa fa-edit'></i></a>
		<a @click=\"setup_overlay(".$row["id"].")\" class='btn btn-secondary' style='background:teal'><i class='fa fa-edit'></i></a>
		<a href='old.php?show=".$row["id"]."&action=category&id=".$_SESSION["category_id"]."' class='btn btn-secondary' target=_blank><i class='fa fa-eye'></i></a>
	</td>";
	echo "</tr>";
}
echo "</tbody></table>";
?>

</div>
</div>
</div>

<script>
var app = new Vue({
	el: '#app',
	methods: {
		setup_overlay: function(movie_id) { this.overlay.movie=this.movie_by_id(movie_id) },
		movie_by_id: function(id) { return this.movies.filter(m=>m.id == id)[0] },
		update: function() {
			this.overlay.message="Laden..."
			fetch("?site=movies&action=update_movie&response=json",{
				method: "post",
				body: JSON.stringify({movie:this.overlay.movie})
			})
			.then(d=>d.json())
			.then(d=>{
				if(d.success) this.overlay.movie=undefined;
				else this.overlay.message="Fehler";
			})
			.catch(e=> this.overlay.message="Unbekant Fehler")
		}
	},
	data: {
		overlay: {
			movie:undefined,
			message:undefined
		},
		faces: [
			"https://img.icons8.com/emoji/452/smiling-face-with-smiling-eyes.png",
			"https://cdn.pixabay.com/photo/2019/02/19/19/45/thumbs-up-4007573_960_720.png",
			"https://i.pinimg.com/originals/32/5e/ce/325eceff32c874a2cf4129c506b0e36b.png",
			"https://image.freepik.com/free-vector/happy-face-emoji-illustration_6735-1057.jpg",
			"https://img.icons8.com/emoji/452/smiling-face-with-smiling-eyes.png",
			"https://freesvg.org/img/Bug-Eyed-Tounge--Arvin61r58.png",
			"https://www.searchpng.com/wp-content/uploads/2019/02/Happy-Face-Emoji-PNG-Image-715x715.png",
			"https://www.papertraildesign.com/wp-content/uploads/2017/06/emoji-sunglasses.png",
			"https://cdn.shopify.com/s/files/1/1061/1924/products/Smiling_Face_with_Halo_grande.png?v=1571606036",
			"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUwxYANcQeRm6l3LxUng9XZy74Pw0_bJBMYQ&usqp=CAU",
			"https://co.currituck.nc.us/wp-content/uploads/exercise-happy-face.png",
			"https://wowbestpics.com/wp-content/uploads/2019/12/15875-illustration-of-an-orange-smiley-face-pv.png",
			"https://creazilla-store.fra1.digitaloceanspaces.com/cliparts/17393/happy-face-penguin-clipart-md.png",
			"https://cdn4.iconfinder.com/data/icons/beagle-1/512/beagle_sticker_smiley_emoji_emoticon_harry_potter-512.png",
			"https://www.iconarchive.com/download/i107381/google/noto-emoji-animals-nature/22278-owl.ico",
			"https://i.gifer.com/3wm5.gif",
			"https://cdn4.iconfinder.com/data/icons/teddy-bear-1/512/sticker_emoji_emoticon_teddy_smiley_harry_potter-512.png",
			"https://i.pinimg.com/originals/63/70/1e/63701eb4671cbbe4e85f082f291d574f.png",
			"https://media2.giphy.com/media/XHVerewyNrv5xswcfz/giphy.gif",
			"https://www.pngkit.com/png/full/276-2766983_choose-safer-sex-options-happy-face-emoji-icon.png",
			"https://cdn.pixabay.com/photo/2012/04/18/18/37/toy-37522_960_720.png",
			"https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/03665ef2-aec1-4837-b533-7cd019e2e72a/d6o2ot5-b2bf55c4-4f12-446f-bd5f-5477b6a2e0da.png/v1/fill/w_894,h_894,strp/a_happy_smiling_face_by_vigorousjammer_d6o2ot5-pre.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD0xMjIwIiwicGF0aCI6IlwvZlwvMDM2NjVlZjItYWVjMS00ODM3LWI1MzMtN2NkMDE5ZTJlNzJhXC9kNm8yb3Q1LWIyYmY1NWM0LTRmMTItNDQ2Zi1iZDVmLTU0NzdiNmEyZTBkYS5wbmciLCJ3aWR0aCI6Ijw9MTIyMSJ9XV0sImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl19.s6gDgNVf93vUZEvi6vwMzqcYjwJT9m26l8aWvG5zCKY",
			"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/SNice.svg/1200px-SNice.svg.png"
		] ,
		movies: <?php echo json_encode($movies);?>
	}
})
</script>

<style>
#overlay-edit {
	position:fixed;
	top:0;right:0;	
	left:0;bottom:0;
	z-index:999;
	-webkit-backdrop-filter: blur(10px);
	background:#0009;
} #overlay-edit form {
	max-width:700px;
	margin:auto;
	width:100%;
	margin-top:5%;
	background:#fffb;
	border-radius:10px;
	-webkit-backdrop-filter: blur(10px);
	padding:20px;
}
body {
	background-attachment:fixed;
	background-image:url(https://images.pexels.com/photos/189349/pexels-photo-189349.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260);
	background-size:cover;
}
.card {
	border:none;padding:0;
	background:none
}

.form-control ,.form-select,.bg { background:#fffc;margin-bottom:3px }
.form-control:focus ,.form-select:focus { background:#fffe }
</style>


